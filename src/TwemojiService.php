<?php
namespace Avris\Twemoji;

use Symfony\Component\Cache\CacheItem;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class TwemojiService
{
    /** @var array */
    private $emojis;

    public function __construct(
        HttpClientInterface $httpClient,
        CacheInterface $cache,
        $format = 'svg',
        $version = 'latest',
        $repo = 'jdecked/twemoji',
        $cdn = 'https://cdn.jsdelivr.net/npm/@twemoji/svg'
    )
    {
        $this->emojis = $cache->get('twemoji', function (CacheItem $item) use ($httpClient, $format, $version, $repo, $cdn) {
            $reponse = $httpClient->request(
                'GET',
                sprintf('https://api.github.com/repos/%s/git/trees/main?recursive=1', $repo),
                [
                    'headers' => [
                        'Accept' => 'application/vnd.github.v3+json',
                    ],
                ]
            );

            $emojis = [];
            foreach ($reponse->toArray()['tree'] as $element) {
                if (!preg_match('#^assets/svg/([0-9a-z-]+).svg$#', $element['path'], $matches)) {
                    continue;
                }
                $emoji = '';
                foreach (explode('-', $matches[1]) as $code) {
                    $emoji .= mb_chr(hexdec($code));
                }
                $emojis[$emoji] = sprintf(
                    '<img draggable="false" class="emoji" alt="%s" src="%s@%s/%s.%s">',
                    $emoji,
                    $cdn,
                    $version,
                    $matches[1],
                    $format
                );
            }

            $item->expiresAfter(30 * 24 * 60 * 60);

            return $emojis;
        });
    }

    public function replace(string $string): string
    {
        $isTag = false;
        $buffer = '';
        $output = '';

        foreach (mb_str_split($string) as $char) {
            if (!$isTag && $char === '<') {
                $output .= strtr($buffer, $this->emojis);
                $buffer = '<';
                $isTag = true;
                continue;
            }

            if ($isTag && $char === '>') {
                $output .= $buffer . '>';
                $buffer = '';
                $isTag = false;
                continue;
            }

            $buffer .= $char;
        }

        $output .= $isTag ? $buffer : strtr($buffer, $this->emojis);

        return $output;
    }
}
