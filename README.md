# Avris Twemoji

Backend-generated Twitter Emoji

[Twemoji](https://github.com/twitter/twemoji) is a great way to make emoji's on your website independent of system and browser.
But alas, it requires JavaScript...

Unless you just use this library to replace emojis with `<img>` tags in your backend.

Note: it will, of course, increase the server response time (instead removing a flash of system emojis before JS loads).
Therefore, it's better suited for generated static websites or HTTP cached requests.

## Installation

    composer req avris/twemoji
   
If you're using Symfony with autowiring, just register the service(s):

    Avris\Twemoji\TwemojiService: ~
    Avris\Twemoji\TwemojiExtension: ~

## Usage

    $twemoji->replace('Hello! 👋');
    // Hello! <img draggable="false" class="emoji" alt="👋" src="https://twemoji.maxcdn.com/v/12.1.4/svg/1f44b.svg">
    
Or using Twig:

    {% filter twemoji %}
        <p>
            Hello! 👋
        </p>
    {% endfilter %}

## Tests

    vendor/bin/phpunit
